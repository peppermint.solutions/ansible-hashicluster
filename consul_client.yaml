---
- name: Setup consul client
  hosts: nomad_server:nomad_client:vault_server:loadbalancer_server
  vars:
    encrypt_traffic: "{{ consul_encrypt_traffic |d(false) }}"
    serf_port: "{{ consul_port_serf_lan |d(8301) }}"
    server_port: "{{ consul_port_server |d(8300) }}"
    https_port: -1
    grpc_tls_port: -1
    vault_addr: "{{ vault_uri |d('http://127.0.0.1:8200') }}"
    service_name: "{{ consul_client_service_name |d('consul') }}"
    data_dir: "{{ consul_client_data_dir |d('/opt/consul/client/data') }}"
    config_dir: "{{ consul_client_config_dir |d('/opt/consul/client/config') }}"
    config_file: "{{ consul_client_config_file |d('consul.hcl') }}"
    auto_config: "{{ consul_auto_config |d(false) }}"
    acl_enabled: "{{ consul_acl_enabled |d(false) }}"
    acl_default_policy: "{{ consul_acl_default_policy |d('allow') }}"
    agent_token: "{{ consul_tokens.agent }}"
    ui_enabled: "{{ consul_ui_enabled |d(false) }}"
    firewall_ports: "{{ consul_client_firewall_ports |d([]) }}"
    configure_firewalls: "{{ internal_configure_firewalls |d(false) }}"
    initial_management_token: >-
      "{{ _initial_management_token.data.data.key }}"
    agent_token: "{{ initial_management_token }}"

  tasks:
    - name: Lookup initial management token in vault
      community.hashi_vault.vault_kv1_get:
        path: data/consul/initial_management_token
        engine_mount_point: bootstrapper_kv
      register: _initial_management_token

    - name: Install docker yum repo
      yum_repository:
        name: docker-ce-stable
        description: Docker CE Stable - $basearch
        baseurl: https://download.docker.com/linux/centos/$releasever/$basearch/stable
        enabled: yes
        gpgcheck: yes
        gpgkey: https://download.docker.com/linux/centos/gpg

    - name: Install necessary packages
      package:
        name:
          - docker-ce
          - docker-ce-cli
          - containerd.io
          - docker-compose-plugin
        state: present

    - name: Add docker config dir
      file:
        name: /etc/docker
        state: directory

    - name: Add docker registry mirror settings
      template:
        src: templates/docker_daemon.json.j2
        dest: /etc/docker/daemon.json

    - name: Start docker service
      systemd:
        name: docker
        state: started
        enabled: yes

    - name: Add cluster members to firewalld zone
      firewalld:
        zone: internal
        source: "{{ internal_network }}"
        permanent: true
        immediate: true
        state: enabled
      when: configure_firewalls is defined and configure_firewalls is true

    - name: Add firewalld ports to zone
      firewalld:
        zone: internal
        port: "{{ item.port }}/{{ item.proto }}"
        permanent: true
        immediate: true
        state: enabled
      loop: "{{ firewall_ports }}"
      when: configure_firewalls is defined and configure_firewalls is true

    - name: Set grpc tls port if encryption is turned on
      set_fact:
        grpc_tls_port: 8503
      when: consul_encrypt_traffic is defined and consul_encrypt_traffic

    - name: Set http port if encryption is turned on
      set_fact:
        http_port: -1
      when: consul_encrypt_traffic is defined and consul_encrypt_traffic

    - name: Set https port if encryption is turned on
      set_fact:
        https_port: 8501
      when: consul_encrypt_traffic is defined and consul_encrypt_traffic

    - name: Add consul group
      group:
        name: consul
        gid: 1000
        state: present

    - name: Add consul user
      user:
        name: consul
        uid: 100
        group: consul
        state: present

    - name: Add consul client config dir
      file:
        name: "{{ config_dir }}"
        state: directory
        force: yes
        owner: 100
        group: 1000
        mode: "0775"

    - name: Set perms on consul client config file
      copy:
        content: This content should be replaced by consul-template
        dest: "/tmp/{{ config_file }}"
        owner: 100
        group: 1000
        mode: "0664"

    - name: Copy consul-template config files to config dir
      template:
        src: templates/{{ item }}.j2
        dest: /etc/consul-template/config.d/{{ item | basename }}
        owner: root
        group: root
        mode: "0664"
      loop:
        - consul-template/consul-client-config
      notify: Reload consul-template

    - name: Copy consul-template templates to template dir
      template:
        src: templates/{{ item }}.j2
        dest: /etc/consul-template/templates/{{ item | basename }}
        owner: root
        group: root
        mode: "0664"
      loop:
        - consul-template/consul-client.hcl.tpl
      notify: Reload consul-template

    - name: Add consul data dir
      file:
        dest: "{{ data_dir }}"
        state: directory
        owner: 100
        group: 1000
        mode: "0777"

    - name: Add consul client systemd service file
      template:
        src: templates/consul_client_service_file.j2
        dest: /usr/lib/systemd/system/consul-client.service
        owner: root
        group: root
        mode: "0644"
      notify: Reload systemd

    - name: Add consul client service override directory
      file:
        name: /etc/systemd/system/consul-client.service.d
        state: directory
      notify: Reload systemd

    - name: Add consul client service override file
      template:
        src: templates/consul_client_systemd_override.j2
        dest: /etc/systemd/system/consul-client.service.d/override.conf
      notify: Reload systemd

    - name: Debug output bootstrapper_token
      copy:
        content: "{{ bootstrapper_token }}"
        dest: /tmp/jwt-access-token

    - name: Get auto_config JWT
      uri:
        url: "{{ vault_addr }}/v1/identity/oidc/token/consul-auto-config"
        return_content: "yes"
        headers:
          Authorization: "Bearer {{ bootstrapper_token }}"
      register:
        jwt_response

    - name: Extract JWT from response
      set_fact:
        auto_config_jwt: >-
          {{ jwt_response.json
          |json_query('data.token') }}

    - name: Create JWT storage directory
      file:
        path: /opt/consul/client/tokens
        state: directory

    - name: Store wrapping token response output
      copy:
        dest: /opt/consul/client/tokens/jwt
        content: "{{ auto_config_jwt }}"

    - name: Debug to file
      copy:
        dest: /tmp/consul-client-token
        content: >
          {{ labels }}

#    - name: Wait for consul cluster to be configured
#      uri:
#        url: https://127.0.0.1:18501/v1/operator/raft/configuration
#      register: consul_cluster_state
#      until: consul_cluster_state is not failed
#      retries: 10
#      delay: 10
#      when: >-
#        "consul" in labels and
#        "server" in labels.consul

    - name: Start and enable consul client systemd service
      systemd:
        name: consul-client
        enabled: yes
      notify: Start consul-client

    - name: Flush handlers
      meta: flush_handlers

    - name: Register agent token with client
      uri:
        url: http://127.0.0.1:8500/v1/agent/token/acl_token
        method: PUT
        headers:
          "Authorization": "Bearer {{ initial_management_token[1:-1] }}"
        body_format: json
        body:
          Token: "{{ agent_token[1:-1] }}"
      register: acl_token_registered
      until: acl_token_registered is not failed
      retries: 20
      delay: 15
      when: >
        acl_enabled and
        agent_token is defined and
        initial_management_token is defined

    - name: Debug agent registering
      copy:
        content: "{{ acl_token_registered }}"
        dest: /tmp/acl_token_registered

  handlers:
    - name: Reload systemd
      systemd:
        daemon_reload: yes

    - name: Reload consul-template
      service:
        name: consul-template
        state: reloaded

    - name: Start consul-client
      service:
        name: consul-client
        state: started
