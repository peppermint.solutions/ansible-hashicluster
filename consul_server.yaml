---
- name: Setup consul server
  hosts: consul_server
  vars:
    server_port: "{{ consul_port_server |d(8300) }}"
    grpc_tls_port: "{{ consul_port_grpc_tls |d(-1) }}"
    serf_lan_port: "{{ consul_port_serf_lan |d(8301) }}"
    https_port: -1
    connect_token: "{{ consul_connect_token |d('') }}"
    vault_addr: "{{ vault_uri |d('http://127.0.0.1:8200') }}"
    service_name: "{{ consul_server_service_name |d('consul') }}"
    data_dir: "{{ consul_server_data_dir |d('/opt/consul/data') }}"
    config_dir: "{{ consul_server_config_dir |d('/etc/consul.d') }}"
    config_file: "{{ consul_server_config_file |d('consul.hcl') }}"
    env_file: "{{ consul_env_file |d('consul.env') }}"
    service_file: "{{ consul_server_service_file |d('/usr/lib/systemd/system/consul.service') }}"
    acl_enabled: "{{ consul_acl_enabled |d(false) }}"
    acl_default_policy: "{{ consul_acl_default_policy |d('allow') }}"
    agent_token: "{{ consul_tokens.agent }}"
    leaf_cert_ttl: "{{ consul_leaf_cert_ttl |d('72h') }}"
    intermediate_cert_ttl: "{{ consul_intermediate_cert_ttl |d('8760h') }}"
    proxy_defaults: "{{ consul_proxy_defaults |d(false) }}"
    encrypt_traffic: "{{ consul_encrypt_traffic |d(false) }}"
    auto_config: "{{ consul_auto_config |d(false) }}"
    cluster_firewall_ports: "{{ consul_cluster_firewall_ports }}"
    configure_firewalls: "{{ internal_configure_firewalls |d(false) }}"
    initial_management_token: "{{ _initial_management_token.data.data.key }}"

  tasks:
    - name: Lookup initial management token in vault
      community.hashi_vault.vault_kv1_get:
        path: data/consul/initial_management_token
        engine_mount_point: bootstrapper_kv
      register: _initial_management_token

    - name: Debug kv1 lookup
      copy:
        content: "{{ _initial_management_token.data.data.key }}"
        dest: /tmp/vault_kv1_get

    - name: Add consul group
      group:
        name: consul
        gid: 1000
        state: present

    - name: Add consul user
      user:
        name: consul
        uid: 100
        group: consul
        state: present

    - name: Install required packages
      package:
        name: "{{ package_list }}"
        state: present
      vars:
        package_list:
          - consul

    - name: Add cluster members to firewalld zone
      firewalld:
        zone: internal
        source: "{{ internal_network }}"
        permanent: true
        immediate: true
        state: enabled
      when: configure_firewalls is defined and configure_firewalls is true

    - name: Add firewalld ports to zone
      firewalld:
        zone: internal
        port: "{{ item.port }}/{{ item.proto }}"
        permanent: true
        immediate: true
        state: enabled
      loop: "{{ cluster_firewall_ports }}"
      when: configure_firewalls is defined and configure_firewalls is true

    - name: Debug to file
      copy:
        dest: /tmp/consul-client-ips
        content: >
          {{ consul_client_ips |d("Not the one") }}

    - name: Add consul server config dir
      file:
        name: "{{ config_dir }}"
        state: directory
        force: yes
        owner: root
        group: consul
        mode: "0770"

    - name: Set grpc_tls value dynamically
      set_fact:
        grpc_tls_port: 18503
      when: consul_encrypt_traffic is defined and consul_encrypt_traffic

    - name: Set http value dynamically
      set_fact:
        http_port: -1
      when: consul_encrypt_traffic is defined and consul_encrypt_traffic

    - name: Set https value dynamically
      set_fact:
        https_port: 18501
      when: consul_encrypt_traffic is defined and consul_encrypt_traffic

    - name: Debug output consul encryption key
      copy:
        dest: /tmp/consul_encryption_key
        content: "{{ consul_encrypt_key }}"

    - name: Copy consul-template configuration to config dir
      template:
        src: templates/{{ item }}.j2
        dest: /etc/consul-template/config.d/{{ item | basename }}
        owner: root
        group: root
        mode: "0664"
      loop:
        - consul-template/consul-server-config
      notify: Reload consul-template

    - name: Copy consul-template templates to config dir
      template:
        src: templates/{{ item }}.j2
        dest: /etc/consul-template/templates/{{ item | basename }}
        owner: root
        group: root
        mode: "0664"
      loop:
        - consul-template/consul-server.hcl.tpl
      notify: Reload consul-template

    # FIXME: This should be done using the meta handler thingie
    - name: Reload consul-template
      service:
        name: consul-template
        state: reloaded

    - name: Add consul config file
      template:
        src: templates/consul_server.hcl.j2
        dest: "/tmp/{{ config_file }}"
        owner: root
        group: consul
        mode: "0660"

    - name: Make sure consul data dir exists
      file:
        name: "{{ data_dir }}"
        state: directory
        force: yes
        owner: root
        group: consul
        mode: 0770

    - name: Make sure consul server tls dir exists
      file:
        name: "{{ data_dir }}/tls"
        state: directory
        force: yes
        owner: root
        group: consul
        mode: 0750

    - name: Check if consul systemd service exists in /etc
      stat:
        path: "{{ service_file }}"
      register: service_file_check

    - name: Copy consul systemd service to /etc for modification
      copy:
        src: /usr/lib/systemd/system/consul.service
        dest: "{{ service_file }}"
        remote_src: yes
      when: not service_file_check.stat.exists
      notify: Reload systemd

    - name: Change ExecStart for consul service
      lineinfile:
        path: "{{ service_file }}"
        regexp: '^(ExecStart=.+-config-dir=).*'
        line: \g<1>{{ config_dir }}
        backrefs: yes
      notify: Reload systemd

    - name: Change ConditionFileNotEmpty to correct config file
      lineinfile:
        path: "{{ service_file }}"
        regexp: '^(ConditionFileNotEmpty=).*'
        line: \g<1>{{ config_dir }}/{{ config_file }}
        backrefs: yes
      notify: Reload systemd

    - name: Change EnvironmentFile to correct config dir
      lineinfile:
        path: "{{ service_file }}"
        regexp: '^(EnvironmentFile=-).*'
        line: \g<1>{{ config_dir }}/{{ env_file }}
        backrefs: yes
      notify: Reload systemd

    - name: Add consul service override directory
      file:
        name: /etc/systemd/system/consul.service.d
        state: directory

    - name: Add consul service override file
      template:
        src: templates/consul_server_systemd_override.j2
        dest: /etc/systemd/system/consul.service.d/override.conf
      notify: Reload systemd

    - name: Start and enable consul systemd service
      systemd:
        name: consul
        state: started
        enabled: yes

    - name: Set proxy-defaults from one machine only
      block:
#        - name: Check if consul proxy-defaults config exists
#          uri:
#            url: http://127.0.0.1:18500/v1/config/proxy-defaults/global
#          register:
#            _proxy_defaults_check
#          retries: 720
#          delay: 5
#          until: _proxy_defaults_check is not failed

        - name: Set consul proxy-defaults config if unset
          uri:
            url: http://127.0.0.1:18500/v1/config
            headers:
              Authorization: "Bearer {{ initial_management_token }}"
            method: PUT
            body_format: json
            body: "{{ proxy_defaults }}"
          register:
            _proxy_defaults_set
          retries: 720
          delay: 5
          until: _proxy_defaults_set is not failed
      when: consul_cluster | sort | first == ipv4 and proxy_defaults

    - name: Debug log
      copy:
        dest: /tmp/variables
        content: >
          {{ consul_cluster | sort | first }}
          {{ ipv4 }}

  handlers:
    - name: Reload systemd
      systemd:
        daemon_reload: yes

    - name: Reload consul-template
      service:
        name: consul-template
        state: reloaded

    - name: Reload firewalld
      service:
        name: firewalld
        state: reloaded
