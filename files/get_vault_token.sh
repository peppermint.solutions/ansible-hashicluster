export VAULT_TOKEN=$(curl --request POST \
                          --cert /etc/pki/tls/certs/hostcert.pem \
                          --key /etc/pki/tls/private/hostkey.pem \
                          ${VAULT_ADDR}/v1/auth/cert/login | jq -r .auth.client_token)
