job "dns" {
  datacenters = ["dc1"]

  update {
    max_parallel = 1
    min_healthy_time = "1m"
    auto_revert = true
  }

  group "nsd" {
    network {
      mode = "bridge"
      port "dns" {
        to = 53
      }
    }

    service {
      name = "dns"
      port = "dns"

      connect {
        sidecar_service {}
      }
    }

    count = 2

    task "nsd-external" {
      driver = "docker"

      config {
        image = "10.0.0.2:5000/mwinther/nsd:latest"
      }

      template {
        data = "server:\n  database: \"\" # >>{{ keyOrDefault \"nsd/config/server/database\" \"default\" }}<<\n  pidfile: {{ env \"NOMAD_TASK_DIR\" }}/nsd.pid\n\nremote-control:\n  control-enable: yes\n  control-interface: /var/run/nsd.sock\n\nzone:\n  name: example.com\n  zonefile: {{ env \"NOMAD_TASK_DIR\" }}/%s.zone\n"
        destination = "${NOMAD_TASK_DIR}/nsd.conf"
      }

      template {
        data = <<EOH
$ORIGIN             example.com.
$TTL    300
@       3600  SOA   ns1.example.com. hostmaster.example.com. (
2018121401  ; serial YYYYMMDDnn
1440        ; refresh
3600        ; retry
604800      ; expire
300 )       ; minimum TTL
@             NS    ns1.example.com.
@             NS    ns2.example.com.
ns1           A     192.168.0.12
ns2           A     192.168.0.13
@             MX    10 smtp.example.com.
@             MX    20 smtp.example.com.
@             A     192.168.0.12
www           A     192.168.0.12
EOH

        destination = "${NOMAD_TASK_DIR}/example.com.zone"
      }

      template {
        data = "{{ tree \"nsd/config\" | explode | toYAML | replaceAll \"'\" \"\" }}\n"
        destination = "/local/explode.foo"
      }

      env {
        NSD_CONF = "${NOMAD_TASK_DIR}/nsd.conf"
      }
    }
  }
}
