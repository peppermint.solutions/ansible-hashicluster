job "fakeservice" {
  datacenters = ["dc1"]
  type = "service"
  update {
    max_parallel = 1
    min_healthy_time = "10s"
    healthy_deadline = "3m"
    progress_deadline = "10m"
    auto_revert = false
    canary = 0
  }
  migrate {
    max_parallel = 1
    health_check = "checks"
    min_healthy_time = "10s"
    healthy_deadline = "5m"
  }
  group "frontend" {
    count = 1
    network {
      mode = "bridge"
      port "http" {
        to = 9090
      }
    }
    service {
      name     = ""
      tags     = ["global", "web", "fakeservice"]
      provider = "consul"
      connect {
        sidecar_service {}
      }
    }
    restart {
      attempts = 2
      interval = "30m"
      delay = "15s"
      mode = "fail"
    }
    ephemeral_disk {
      size = 300
    }
    task "web" {
      driver = "docker"
      config {
        image = "10.0.0.2:5000/nicholasjackson/fake-service:v0.24.2"
        auth_soft_fail = true
      }
      env {
        MESSAGE = "Welcome to our home page"
      }
      resources {
        cpu    = 500 # 500 MHz
        memory = 256 # 256MB
      }
    }
  }
  group "api" {
    count = 1
    network {
      mode = "bridge"
      port "http" {
        to = 9090
      }
    }
    service {
      name     = ""
      tags     = ["global", "api", "fakeservice"]
      provider = "consul"
    }
    restart {
      attempts = 2
      interval = "30m"
      delay = "15s"
      mode = "fail"
    }
    ephemeral_disk {
      size = 300
    }
    task "api" {
      driver = "docker"
      config {
        image = "10.0.0.2:5000/nicholasjackson/fake-service:v0.24.2"
        auth_soft_fail = true
      }
      env {
        MESSAGE = "{ \"api\": \"unknown version\" }"

      }
      resources {
        cpu    = 500 # 500 MHz
        memory = 256 # 256MB
      }
    }
  }
  group "backend" {
    count = 1
    network {
      mode = "bridge"
      port "http" {
        to = 9090
      }
    }
    service {
      name     = ""
      tags     = ["global", "backend", "fakeservice"]
      provider = "consul"
    }
    restart {
      attempts = 2
      interval = "30m"
      delay = "15s"
      mode = "fail"
    }
    ephemeral_disk {
      size = 300
    }
    task "backend-api" {
      driver = "docker"
      config {
        image = "10.0.0.2:5000/nicholasjackson/fake-service:v0.24.2"
        auth_soft_fail = true
      }
      env {
        MESSAGE = "{ \"api\": \"unknown backend call\" }"
      }
      resources {
        cpu    = 500 # 500 MHz
        memory = 256 # 256MB
      }
    }
  }
}
