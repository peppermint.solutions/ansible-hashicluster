---
- name: Setup nomad client
  hosts: nomad_client:loadbalancer_server
  vars:
    config_dir: "{{ nomad_config_dir |d('/etc/nomad.d/') }}"
    config_file: "{{ nomad_config_file |d('nomad.hcl') }}"
    acl_enabled: "{{ nomad_acl_enabled |d(false) }}"
    initial_management_token: >-
      "{{ _initial_management_token.data.data.key }}"

  tasks:
    - name: Lookup initial management token in vault
      community.hashi_vault.vault_kv1_get:
        path: data/consul/initial_management_token
        engine_mount_point: bootstrapper_kv
      register: _initial_management_token

    - name: Install nomad and required packages
      package:
        name:
          - nomad
          - consul
        state: present

    - name: Get nomad client token from vault
      uri:
        url: "{{ vault_uri }}/v1/bootstrapper_kv/data/nomad/client"
        headers:
          Authorization: "Bearer {{ bootstrapper_token }}"
        return_content: true
      register: nomad_client_token_result
        #until: nomad_client_token_result.status_code == 200
      retries: 720
      delay: 5

    - name: Debug nomad client token result output
      copy:
        content: "{{ nomad_client_token_result }}"
        dest: /tmp/nomad_client_token_result

    - name: Set nomad client token from vault
      set_fact:
        #client_token: "{{ nomad_client_token_result.json.data.data.token }}"
        client_token: "{{ consul_tokens.agent }}"

    - name: Add nomad client config dir
      file:
        name: "{{ config_dir }}"
        state: directory
        force: yes
        owner: root
        group: nomad
        mode: "0770"

    - name: Set perms on consul client config file
      copy:
        content: This content should be replaced by consul-template
        dest: "/etc/nomad.d/nomad.hcl"
        owner: root
        group: nomad
        mode: "0660"

    - name: Copy consul-template configuration to config dir
      template:
        src: templates/{{ item }}.j2
        dest: /etc/consul-template/config.d/{{ item | basename }}
        owner: root
        group: root
        mode: "0664"
      loop:
        - nomad-certs
        - consul-template/nomad-client-config
      notify: Reload consul-template

    - name: Copy consul-template templates to config dir
      template:
        src: templates/{{ item }}.j2
        dest: /etc/consul-template/templates/{{ item | basename }}
        owner: root
        group: root
        mode: "0664"
      loop:
        - consul-template/nomad-cert.tpl
        - consul-template/nomad-key.tpl
        - consul-template/nomad-ca.tpl
        - consul-template/nomad-client.hcl.tpl
      notify: Reload consul-template

    - name: Add nomad config file
      template:
        src: templates/nomad_client.hcl.j2
        dest: "/tmp/{{ config_file }}"
        owner: root
        group: nomad
        mode: "0660"

    - name: Debug custom fact output
      copy:
        dest: /tmp/private_interface
        content: "{{ ansible_local.ip_interface[ipv4_internal] }}"

    - name: Download cni release file for client architecture
      get_url:
        url: "{{ cni_url }}"
        dest: /tmp/cni-plugins.tgz
        checksum: "{{ cni_checksum }}"

    - name: Create cni bin directory
      file:
        dest: /opt/cni/bin
        state: directory
        force: yes
        mode: "0775"

    - name: Extract cni plugins
      unarchive:
        src: /tmp/cni-plugins.tgz
        dest: /opt/cni/bin
        remote_src: yes

#    - name: Create nomad tls directory
#      file:
#        dest: /opt/nomad/tls
#        state: directory
#        force: yes
#        mode: "0775"
#
#    - name: Get intermediate CA for hostcert
#      uri:
#        url: "{{ vault_uri }}/v1/hetzner_bootstrap/issuer/default"
#        return_content: "yes"
#        headers:
#          Authorization: "Bearer {{ bootstrapper_token }}"
#      register:
#        intermediate_response
#
#    - name: Extract intermediate from response
#      set_fact:
#        hostcert_intermediate: >-
#          {{ intermediate_response.json
#          |json_query('data.ca_chain') }}
#
#    - name: Create output data from json response
#      set_fact:
#        hostcert_output: |
#          {{ hostcert_output |d('') }}
#          {{ item }}
#      loop: "{{ intermediate_response.json |json_query('data.ca_chain') }}"
#
#    - name: Store intermediate response output
#      copy:
#        dest: /opt/nomad/tls/ca.pem
#        content: "{{ hostcert_output }}"
#
#    - name: Symlink host certificate to /opt/nomad/tls
#      file:
#        src: /etc/pki/tls/certs/hostonly.pem
#        dest: /opt/nomad/tls/cert.pem
#        state: link
#
#    - name: Symlink host key to /opt/nomad/tls
#      file:
#        src: /etc/pki/tls/private/hostkey.pem
#        dest: /opt/nomad/tls/key.pem
#        state: link
#
#    - name: Check if nomad systemd service exists in /etc
#      stat:
#        path: "{{ nomad_client_service_file }}"
#      register: nomad_client_service_file_check
#
#    - name: Copy nomad systemd service to /etc
#      copy:
#        src: /usr/lib/systemd/system/nomad.service
#        dest: "{{ nomad_client_service_file }}"
#        remote_src: yes
#      when: not nomad_client_service_file_check.stat.exists
#      notify: Reload systemd
#
#    - name: Change ExecStart for nomad service
#      lineinfile:
#        path: "{{ nomad_client_service_file }}"
#        regexp: '^(ExecStart=.+-config-dir=).*'
#        line: \g<1>{{ nomad_client_config_dir }}
#        backrefs: yes
#      notify: Reload systemd
#
#    - name: Change ConditionFileNotEmpty to correct config file
#      lineinfile:
#        path: "{{ nomad_client_service_file }}"
#        regexp: '^(ConditionFileNotEmpty=).*'
#        line: \g<1>{{ nomad_client_config_file }}
#        backrefs: yes
#      notify: Reload systemd
#
#    - name: Change EnvironmentFile to correct config dir
#      lineinfile:
#        path: "{{ nomad_client_service_file }}"
#        regexp: '^(EnvironmentFile=-).*'
#        line: \g<1>{{ nomad_client_env_file }}
#        backrefs: yes
#      notify: Reload systemd
#
#    - name: Add nomad service override directory
#      file:
#        name: /etc/systemd/system/nomad.service.d
#        state: directory
#
#    - name: Add nomad service override file
#      template:
#        src: templates/nomad_systemd_override.conf.j2
#        dest: /etc/systemd/system/nomad.service.d/override.conf
#      notify: Reload systemd

    - name: Start and enable nomad systemd service
      systemd:
        name: nomad
        enabled: yes
      notify: Start nomad

  handlers:
    - name: Reload consul-template
      service:
        name: consul-template
        state: reloaded

    - name: Start nomad
      service:
        name: nomad
        state: started
